"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const indexController_1 = require("../controllers/indexController");
class IndexRoutes {
    constructor() {
        this.router = express_1.Router();
        this.config();
    }
    config() {

        //get
        this.router.get('/clientes/:id', indexController_1.indexController.getClientes);
        this.router.get('/cliente/:id', indexController_1.indexController.getCliente);
        this.router.get('/producto', indexController_1.indexController.getProductos);
        this.router.get('/producto/:id', indexController_1.indexController.getProducto);
        this.router.post('/login', indexController_1.indexController.Login);
        this.router.get('/inventarios/:id', indexController_1.indexController.getInventarios);
        this.router.post('/inventarios', indexController_1.indexController.getInventario);
        this.router.get('/bodega/:id', indexController_1.indexController.getBodegas);
        this.router.post('/histoinv', indexController_1.indexController.getHistorialInventario);
        this.router.get('/trans1', indexController_1.indexController.getTrans1);
        this.router.get('/rep1', indexController_1.indexController.getRepartidores);
        this.router.get('/repv', indexController_1.indexController.getV);
        this.router.get('/permiso1/:id', indexController_1.indexController.getPermiso1);
        this.router.get('/permiso2', indexController_1.indexController.getPermiso2);
        this.router.get('/desactiva/:id', indexController_1.indexController.getDesactiva);
        this.router.get('/activa/:id', indexController_1.indexController.updateActiva);


        //post        
        this.router.post('/cliente', indexController_1.indexController.createClient);           
        this.router.post('/venta', indexController_1.indexController.createVenta);
        this.router.post('/addproducto', indexController_1.indexController.addProducto);
        this.router.post('/categoria', indexController_1.indexController.createCategoria);
        this.router.post('/producto', indexController_1.indexController.createProduct);
        this.router.get('/permiso/:id', indexController_1.indexController.updatePermiso);
        this.router.post('/inventario', indexController_1.indexController.createInventario);
        this.router.post('/transferencia', indexController_1.indexController.createTransferencia);

        //put       
        this.router.put('/cliente/:id', indexController_1.indexController.updateClient);             
        this.router.put('/venta/:id', indexController_1.indexController.updateVenta);
        this.router.put('/producto/:id', indexController_1.indexController.updateProduct);
        this.router.put('/inventario/:id', indexController_1.indexController.updateInventario);
        this.router.put('/trans1/:id', indexController_1.indexController.updateTransferencia);

        //delete       
        this.router.delete('/cliente/:id', indexController_1.indexController.deleteClient);        
        this.router.delete('/venta/:id', indexController_1.indexController.deleteVenta);
        this.router.delete('/categoria/:id', indexController_1.indexController.deleteCategoria);
        this.router.delete('/producto/:id', indexController_1.indexController.deleteProduct);
        this.router.delete('/inventario/:id', indexController_1.indexController.deleteInventario);

        //=======================================ACTUALIZACIONES CON POST CORREGIDOS=============================================
        //USUARIO
        this.router.get('/', indexController_1.indexController.getUsers); //Ver todos user SI
        this.router.get('/:id', indexController_1.indexController.getUser); //Ver uno user SI
        this.router.post('/user', indexController_1.indexController.createUser);// crear user SI
        this.router.put('/user/:id', indexController_1.indexController.updateUser); //actualizar user SI
        this.router.delete('/user/:id', indexController_1.indexController.deleteUser); // eliminar user SI
        //ROL Y PERMISO
        this.router.get('/rol/:id', indexController_1.indexController.getUserRol);    // Ver rol de un user  SI 
        this.router.post('/newerol', indexController_1.indexController.createRol); // Añadir nuevo rol SI
        //BODEGA
        this.router.get('/verbodegas/bod/bod', indexController_1.indexController.getBodegas); //Ver todas las bodegas NO
        this.router.get('/verbodega/:id', indexController_1.indexController.getBodega); //Ver una bodega NO
        this.router.post('/bodega', indexController_1.indexController.createBodega);//crear una bodega SI
        this.router.put('/bodega/:id', indexController_1.indexController.updateBodega);//actualizar bodega   SI    
        this.router.delete('/bodega/:id', indexController_1.indexController.deleteBodega);//eliminar bodega SI
        //SEDE
        this.router.get('/versedes/sede/sede', indexController_1.indexController.getsedes); //Ver todas las sedes SI
        this.router.get('/versedes/:id', indexController_1.indexController.getsede); //Ver una sede SI
        this.router.post('/sede', indexController_1.indexController.createSede);  //crear sede SI
        this.router.put('/sede/:id', indexController_1.indexController.updateSede);   //actualiziar sede SI
        this.router.delete('/sede/:id', indexController_1.indexController.deleteSede); //eliminar sede SI

        //FUNCIONALEDES DEL USUARIO REPARTIDOR
        //ORDENESDE DE VENTA
        this.router.get('/verventas/ventas/ventas', indexController_1.indexController.getVentas); //Ver todas las venta
        this.router.put('/verventas/ventas/ventas/modi', indexController_1.indexController.ipdateVenta);       //marcar como entregada
        //ORDENES DE TRANSFERENCIA
        this.router.get('/transferencia/transferencia/transferencia', indexController_1.indexController.getTrasnferenciaCesar); //Ver todas las venta
        this.router.put('/transferencia/transferencia/transferencia/modif', indexController_1.indexController.updateTransferenciaCesar);       //marcar como entregada


        //VENTAS CESAR SAZO
        this.router.get('/verventas/verVentaDia/verVentaMes/dia', indexController_1.indexController.getVentaCesarSazo); //Ver ventas por dia o por mes 
        this.router.get('/verventas/verVentaDia/verVentaMes/diaaaa', indexController_1.indexController.getVentaCesarSazo2); //Total

        this.router.get('/verbodegas/bod/bod/bod/bod/bb/hh', indexController_1.indexController.getBodegasCesar); //Ver todas las bodegas NO

        //ELIMINAR ROL
        this.router.get('/rol/rol/rol/rol/rol/rol/rol/rol/rol/rol/rol/rol', indexController_1.indexController.getRolCesar);    // Ver rol de un user  SI 
        this.router.delete('/deleterol/deleterol/deleterol/deleterol/deleterol/deleterol/:id', indexController_1.indexController.deleteRolCesar);//eliminar bodega SI






    }
}
const indexRoutes = new IndexRoutes();
exports.default = indexRoutes.router;
