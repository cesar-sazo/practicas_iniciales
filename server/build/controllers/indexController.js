"use strict";

const { json } = require("express");

var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.indexController = void 0;
const database_1 = __importDefault(require("../database"));
class IndexController {

    createProduct(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                yield database_1.default.query('INSERT INTO Producto set ?', [req.body]);
                res.status(200).send(true);
                console.log(req.body);
            }
            catch (error) {
                res.status(404).send(false);
            }
        });
    }
    updateProduct(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { id } = req.params;
            try {
                yield database_1.default.query('update producto set ? where sku=\"' + id + '\" ', [req.body]);
                //res.send("Se actualizo con exito")
                res.status(200).send(true);
                console.log("se actualizo con exito");
            }
            catch (error) {
                res.status(404).send(false);
            }
            console.log("se actualizo con exito");
        });
    }
    deleteProduct(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { id } = req.params;
            try {
                yield database_1.default.query('delete from producto where sku=\"' + id + '\"');
                //res.send("Se elimino con exito")
                res.status(200).send(true);
                console.log("se elimino con exito");
            }
            catch (error) {
                res.status(404).send(false);
            }
        });
    }
    getProductos(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const usuarios = yield database_1.default.query('SELECT * from producto');
            if (usuarios.length > 0) {
                return res.json(usuarios);
            }
            res.status(404).send(false);
            console.log("No hay ningun producto creado");
        });
    }
    getProducto(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { id } = req.params;
            const usuarios = yield database_1.default.query('SELECT * from producto where sku=\"' + id + '\"');
            if (usuarios.length > 0) {
                return res.json(usuarios);
            }
            res.status(404).send(false);
            console.log("No hay ningun producto con ese nombre");
        });
    }
    createClient(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                yield database_1.default.query('INSERT INTO Cliente set ?', [req.body]);
                res.status(200).send(true);
                console.log(req.body);
            }
            catch (error) {
                res.status(404).send(false);
            }
        });
    }
    updateClient(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { id } = req.params;
            try {
                yield database_1.default.query('update cliente set ? where dpi= ?', [req.body, id]);
                //res.send("Se actualizo con exito")
                res.status(200).send(true);
                console.log("se actualizo con exito");
            }
            catch (error) {
                res.status(404).send(false);
            }
            console.log("se actualizo con exito");
        });
    }
    deleteClient(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { id } = req.params;
            try {
                yield database_1.default.query('delete from cliente where dpi= ?', [id]);
                //res.send("Se elimino con exito")
                res.status(200).send(true);
                console.log("se elimino con exito");
            }
            catch (error) {
                res.status(404).send(false);
            }
        });
    }

    getClientes(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { id } = req.params;
            const usuarios = yield database_1.default.query('SELECT * from cliente where cod_sede= ? ;', [id]);
            if (usuarios.length > 0) {
                return res.json(usuarios);
            }
            res.status(404).send(false);
            console.log("No hay ningun cliente creado");
        });
    }
    getCliente(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { id } = req.params;
            const usuarios = yield database_1.default.query('SELECT * from cliente where dpi= ?', [id]);
            if (usuarios.length > 0) {
                return res.json(usuarios);
            }
            res.status(404).send(false);
            console.log("No hay ningun cliente con ese nombre");
        });
    }

    Login(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const usuarios = yield database_1.default.query('SELECT dpi from usuario where dpi=' + req.body.dpi + ' and pass=\"' + req.body.pass + '\"');
                const rol=yield database_1.default.query('SELECT descripcion from rol where cod_usuario=' + req.body.dpi+ ';');
                console.log(usuarios[0].dpi)
                console.log(rol[0].descripcion)
                var obj=[{"dpi":usuarios[0].dpi,"rol":rol[0].descripcion}]
                return res.send(obj);
            }
            catch (error) {
                res.status(404).send(false);
                console.log("fallo login");
            }
        });
    }

    createCategoria(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                yield database_1.default.query('INSERT INTO Categoria set ?', [req.body]);
                res.status(200).send(true);
                console.log(req.body);
            }
            catch (error) {
                res.status(404).send(false);
            }
        });
    }
    deleteCategoria(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { id } = req.params;
            try {
                yield database_1.default.query('delete from categoria where cod_categoria= ?', [id]);
                //res.send("Se elimino con exito")
                res.status(200).send(true);
                console.log("se elimino con exito");
            }
            catch (error) {
                res.status(404).send(false);
            }
        });
    }
    createVenta(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                yield database_1.default.query('INSERT INTO venta set ?', [req.body]);
                const data1=yield database_1.default.query('select cod_venta from venta order by cod_venta desc limit 1;');
                res.status(200).send(data1);
                console.log(req.body);
            }
            catch (error) {
                res.status(404).send(false);
            }
        });
    }
    addProducto(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                yield database_1.default.query('INSERT INTO detalle set ?', [req.body]);
                res.status(200).send(true);
                console.log(req.body);
            }
            catch (error) {
                res.status(404).send(false);
            }
        });
    }

    updateVenta(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { id } = req.params;
            try {
                yield database_1.default.query('update venta set ? where cod_venta= ?', [req.body, id]);
                //res.send("Se actualizo con exito")
                res.status(200).send(true);
                console.log("se actualizo con exito");
            }
            catch (error) {
                res.status(404).send(false);
            }
            console.log("se actualizo con exito");
        });
    }
    deleteVenta(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { id } = req.params;
            try {
                yield database_1.default.query('delete from venta where cod_venta= ?', [id]);
                //res.send("Se elimino con exito")
                res.status(200).send(true);
                console.log("se elimino con exito");
            }
            catch (error) {
                res.status(404).send(false);
            }
        });
    }
    getInventarios(req,res){
        return __awaiter(this, void 0, void 0, function* () {
            const { id } = req.params;
            const usuarios = yield database_1.default.query('select cod_producto,cod_usuario,cod_bodega,cod_sede,cantigua,cnueva,motivo,fecha from inventario where cod_bodega='+id+' group by cod_producto;'); 
            if (usuarios.length > 0) {
                return res.json(usuarios);
            }
            res.status(404).send(false);
            console.log("No hay ningun producto creado");
        });
    }
    getInventario(req,res){
        return __awaiter(this, void 0, void 0, function* () {
            console.log(req.params)
            const usuarios = yield database_1.default.query('select cod_producto,cod_usuario,cod_bodega,cod_sede,cantigua,cnueva,motivo,fecha from inventario where cod_bodega='+req.body.cod_bodega+' and cod_producto=\"'+req.body.cod_producto+'\" order by cod_inventario desc limit 1;'); 
            if (usuarios.length > 0) {
                return res.json(usuarios);
            }
            res.status(404).send(false);
            console.log("No hay ningun producto creado");
        });
    }
    getHistorialInventario(req, res){
        return __awaiter(this, void 0, void 0, function* () {
            console.log(req.params)
            const usuarios = yield database_1.default.query('select fecha, cantigua, cnueva, motivo from inventario where cod_bodega='+req.body.cod_bodega+' and cod_producto=\"'+req.body.cod_producto+'\" order by cod_inventario desc;'); 
            if (usuarios.length > 0) {
                return res.json(usuarios);
            }
            res.status(404).send(false);
            console.log("No hay ningun producto creado");
        });
    }

    createInventario(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                yield database_1.default.query('INSERT INTO Inventario set ?', [req.body]);
                res.status(200).send(true);
                console.log(req.body);
            }
            catch (error) {
                res.status(404).send(false);
            }
        });
    }
    updateInventario(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { id } = req.params;
            try {
                yield database_1.default.query('update inventario set ? where cod_inventario= ?', [req.body, id]);
                //res.send("Se actualizo con exito")
                res.status(200).send(true);
                console.log("se actualizo con exito");
            }
            catch (error) {
                res.status(404).send(false);
            }
            console.log("se actualizo con exito");
        });
    }
    deleteInventario(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { id } = req.params;
            try {
                yield database_1.default.query('delete from inventario where cod_inventario= ?', [id]);
                //res.send("Se elimino con exito")
                res.status(200).send(true);
                console.log("se elimino con exito");
            }
            catch (error) {
                res.status(404).send(false);
            }
        });
    }
    createTransferencia(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                yield database_1.default.query('INSERT INTO transferencia set ?', [req.body]);
                res.status(200).send(true);
                console.log(req.body);
            }
            catch (error) {
                res.status(404).send(false);
            }
        });
    }
    updateTransferencia(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { id } = req.params;
            try {
                yield database_1.default.query('update transferencia set ? where cod_trans= ?', [req.body, id]);
                //res.send("Se actualizo con exito")
                res.status(200).send(true);
                console.log("se actualizo con exito");
            }
            catch (error) {
                res.status(404).send(false);
            }
            console.log("se actualizo con exito");
        });
    }
    getTrans1(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const usuarios = yield database_1.default.query('SELECT * from Transferencia where aceptada=0;');
            if (usuarios.length > 0) {
                return res.json(usuarios);
            }
            res.status(404).send(false);
            console.log("No hay ningun usuario creado");
        });
    }

    getRepartidores(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const usuarios = yield database_1.default.query('SELECT cod_usuario from rol where descripcion="repartidor";');
            if (usuarios.length > 0) {
                return res.json(usuarios);
            }
            res.status(404).send(false);
            console.log("No hay ningun usuario creado");
        });
    }




    //SERVICIOS DE LAS SEDES
    createSede(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                yield database_1.default.query('INSERT INTO Sede set ?', [req.body]);
                res.status(200).send(true);
                console.log(req.body);
            }
            catch (error) {
                res.status(404).send(false);
            }
        });
    }
    updateSede(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { id } = req.params;
            try {
                yield database_1.default.query('update Sede set ? where cod_sede= ?', [req.body, id]);
                res.status(200).send(true);
                console.log("se actualizo con exito");
            }
            catch (error) {
                res.status(404).send(false);
            }
            console.log("se actualizo con exito");
        });
    }
    deleteSede(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { id } = req.params;
            try {
                yield database_1.default.query('delete from Sede where cod_sede= ?', [id]);
                //res.send("Se elimino con exito")
                res.status(200).send(true);
                console.log("se elimino con exito");
            }
            catch (error) {
                res.status(404).send(false);
            }
        });
    }
    getsedes(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const usuarios = yield database_1.default.query('SELECT * from Sede');
            if (usuarios.length > 0) {
                return res.json(usuarios);
            }
            res.status(404).send(false);
            console.log("No hay ningun usuario creado");
        });
    }
    getsede(req, res) {
        const { id } = req.params;
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield database_1.default.query('SELECT * from Sede where cod_sede = ?', [id]);
            if (data.length > 0) {
                return res.json(data);
            }
            res.status(404).send(false);
            console.log("El usuario no existe");
        });
    }
    //SERCICIOS DE ROLES Y PERMISOS
    createRol(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                yield database_1.default.query('INSERT INTO Rol set ?', [req.body]);
                res.status(200).send(true);
                console.log(req.body);
            }
            catch (error) {
                res.status(404).send(false);
            }
        });
    }

    getUserRol(req, res) {
        const { id } = req.params;
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield database_1.default.query('SELECT * from Rol where cod_usuario = ?', [id]);
            if (data.length > 0) {
                return res.json(data);
            }
            res.status(404).send(false);
            console.log("El usuario no existe");
        });
    }
    //SERVICIOS DE USUARIOS
    getUsers(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const usuarios = yield database_1.default.query('SELECT * from Usuario');
            if (usuarios.length > 0) {
                return res.json(usuarios);
            }
            res.status(404).send(false);
            console.log("No hay ningun usuario creado");
        });
    }
    getUser(req, res) {
        const { id } = req.params;
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield database_1.default.query('SELECT * from Usuario where dpi = ?', [id]);
            if (data.length > 0) {
                return res.json(data);
            }
            res.status(404).send(false);
            console.log("El usuario no existe");
        });
    }
    createUser(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                yield database_1.default.query('INSERT INTO Usuario set ?', [req.body]);
                res.status(200).send(true);
                console.log(req.body);
            }
            catch (error) {
                res.status(404).send(false);
            }
        });
    }
    updateUser(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { id } = req.params;
            try {
                yield database_1.default.query('update Usuario set ? where dpi= ?', [req.body, id]);
                //res.send("Se actualizo con exito")
                res.status(200).send(true);
                console.log("se actualizo con exito");
            }
            catch (error) {
                res.status(404).send(false);
            }
            console.log("se actualizo con exito");
        });
    }
    deleteUser(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { id } = req.params;
            try {
                yield database_1.default.query('delete from Usuario where dpi= ?', [id]);
                //res.send("Se elimino con exito")
                res.status(200).send(true);
                console.log("se elimino con exito");
            }
            catch (error) {
                res.status(404).send(false);
            }
        });
    }
    //SERVICIOS DE BODEGAS
    getBodegas(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { id } = req.params;
            const usuarios = yield database_1.default.query('SELECT * from Bodega where estado=1 and cod_usuario= ?',[id]);
            if (usuarios.length > 0) {
                return res.json(usuarios);
            }
            res.status(404).send(false);
            console.log("No hay ningun usuario creado");
        });
    }
    getBodega(req, res) {
        const { id } = req.params;
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield database_1.default.query('SELECT * from Bodega where estado=1 and cod_bodega = ?', [id]);
            if (data.length > 0) {
                return res.json(data);
            }
            res.status(404).send(false);
            console.log("El usuario no existe");
        });
    }
    createBodega(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                yield database_1.default.query('INSERT INTO Bodega set ?', [req.body]);
                res.status(200).send(true);
                console.log(req.body);
            }
            catch (error) {
                res.status(404).send(false);
            }
        });
    }
    updateBodega(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { id } = req.params;
            try {
                yield database_1.default.query('update Bodega set ? where cod_bodega= ?', [req.body, id]);
                //res.send("Se actualizo con exito")
                res.status(200).send(true);
                console.log("se actualizo con exito");
            }
            catch (error) {
                res.status(404).send(false);
            }
            console.log("se actualizo con exito");
        });
    }
    deleteBodega(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { id } = req.params;
            try {
                yield database_1.default.query('delete from Bodega where cod_bodega= ?', [id]);
                //res.send("Se elimino con exito")
                res.status(200).send(true);
                console.log("se elimino con exito");
            }
            catch (error) {
                res.status(404).send(false);
            }
        });
    }
    getV(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const usuarios = yield database_1.default.query('SELECT * from Venta');
            if (usuarios.length > 0) {
                return res.json(usuarios);
            }
            res.status(404).send(false);
            console.log("No hay ningun usuario creado");
        });
    }

    //FUNCIONALEDES DEL USUARIO REPARTIDOR
    //ORDENESDE DE VENTA
    getVentas(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const usuarios = yield database_1.default.query('SELECT * from Venta where estado = 0 ');
            if (usuarios.length > 0) {
                return res.json(usuarios);
            }
            res.status(404).send(false);
            console.log("No hay ningun usuario creado");
        });
    }

    ipdateVenta(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { id } = req.params;
            try {
                yield database_1.default.query('update Venta set estado = 1 where cod_venta= '+req.body.cod_venta);
                //res.send("Se actualizo con exito")
                res.status(200).send(true);
                console.log("se actualizo con exito");
            }
            catch (error) {
                res.status(404).send(false);
            }
            console.log("se actualizo con exito");
        });
    }

    //ORDENES DE TRANSFERENCIA
    getTrasnferenciaCesar(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const usuarios = yield database_1.default.query('SELECT * from transferencia where estado = 0 and aceptada=1 ');
            if (usuarios.length > 0) {
                return res.json(usuarios);
            }
            res.status(404).send(false);
            console.log("No hay ningun trasnferenciai creado");
        });
    }


    updateTransferenciaCesar(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { id } = req.params;
            try {
                yield database_1.default.query('update transferencia set estado = 1 where cod_trans= '+req.body.cod_trans);
                //res.send("Se actualizo con exito")
                res.status(200).send(true);
                console.log("se actualizo con exito");
            }
            catch (error) {
                res.status(404).send(false);
            }
            console.log("se actualizo con exito");
        });
    }

    updatePermiso(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { id } = req.params;
            try {
                yield database_1.default.query('update usuario set permiso = 1 where dpi= ? ',[id]);
                //res.send("Se actualizo con exito")
                res.status(200).send(true);
                console.log("se actualizo con exito");
            }
            catch (error) {
                res.status(404).send(false);
            }
            console.log("se actualizo con exito");
        });
    }

    getPermiso1(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { id } = req.params;
            const usuarios = yield database_1.default.query('select * from usuario where permiso=1 and dpi= ?',[id]);
            if (usuarios.length > 0) {
                return res.send(true);
            }else{
                return res.send(false);
            }
        });
    }
    getPermiso2(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const usuarios = yield database_1.default.query('select * from usuario where permiso=0');
            if (usuarios.length > 0) {
                return res.json(usuarios);
            }else{
                return res.send(false);
            }
        });
    }
    getDesactiva(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { id } = req.params;
            const usuarios = yield database_1.default.query('select * from bodega where estado=0 and cod_usuario= ?',[id]);
            if (usuarios.length > 0) {
                return res.send(usuarios);
            }
            res.status(404).send(false);
        });
    }
    updateActiva(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { id } = req.params;
            try {
                yield database_1.default.query('update bodega set estado=1 where cod_bodega= ?',[id]);
                //res.send("Se actualizo con exito")
                res.status(200).send(true);
                console.log("se actualizo con exito");
            }
            catch (error) {
                res.status(404).send(false);
            }
            console.log("se actualizo con exito");
        });
    }

    getVentaCesarSazo(req, res) {
        const { id } = req.params;
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield database_1.default.query('SELECT * from venta where fecha = \' '+ req.body.fecha +'\'');
            if (data.length > 0) {
                return res.json(data);
            }
            res.status(404).send(false);
            console.log("El usuario no existe");
        });
    }


    getVentaCesarSazo2(req, res) {
        const { id } = req.params;
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield database_1.default.query('SELECT SUM(total) from venta where fecha = \' ' + req.body.fecha +'\'');
            if (data.length > 0) {
                return res.json(data);
            }
            res.status(404).send(false);
            console.log("El usuario no existe");
        });
    }

    //SERVICIOS DE BODEGAS
    getBodegasCesar(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { id } = req.params;
            const usuarios = yield database_1.default.query('SELECT * from Bodega');
            if (usuarios.length > 0) {
                return res.json(usuarios);
            }
            res.status(404).send(false);
            console.log("No hay ningun usuario creado");
        });
    }

    getRolCesar(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const usuarios = yield database_1.default.query('SELECT * from rol');
            if (usuarios.length > 0) {
                return res.json(usuarios);
            }
            res.status(404).send(false);
            console.log("No hay ningun usuario creado");
        });
    }

    deleteRolCesar(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { id } = req.params;
            try {
                yield database_1.default.query('delete from rol where cod_usuario= ?', [id]);
                //res.send("Se elimino con exito")
                res.status(200).send(true);
                console.log("se elimino con exito");
            }
            catch (error) {
                res.status(404).send(false);
            }
        });
    }

   


}
exports.indexController = new IndexController();
